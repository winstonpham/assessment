import requests

def getWeather(cityid):
    connectionString = (
    f"https://api.openweathermap.org//data/2.5/weather?id={cityid}&units=metric&appid=244df0cbb2bcae2bca2fbe929ae3a613")
    r = requests.get(connectionString)

    return r.json()
