# system imports
import os
import json
from flask import Flask, render_template,request,redirect

# local imports
import api

app = Flask(__name__)
app.config['DEBUG'] = True


@app.route('/')
def home():

    return render_template("home.html")

@app.route('/', methods=['POST'])
def home_submit():
    city_id = request.form['City_id']

    return redirect(f"weather/city/{city_id}")


@app.route('/weather/city/<city_id>', methods=['GET'])


def check_existed(city_id):

       cache_folder = os.listdir("cache")

       if os.path.exists(f"cache/{city_id}.json"):

           resultJson = json.load(open(f"cache/{city_id}.json"))
           return resultJson

       else:
           resultJson = api.getWeather(city_id)
           save_Dict = {
               "temp": str(resultJson['main']['temp']),
               "description": resultJson['weather'][0]['description']
           }

           out_file = json.dumps(save_Dict)
           with open(f"cache/{city_id}.json", "w") as newfile:
               newfile.write(out_file)
               newfile.close()

           return save_Dict


if __name__ == "__main__":
    app.run(host='localhost', port=1234)
